variable "instance_id" {
    type = string
}

variable "device_name" {
    type = string
}

variable "ebs_volume_name" {
    type = string
}