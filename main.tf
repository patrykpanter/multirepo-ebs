# Should be moved outside of EC2
resource "aws_volume_attachment" "ebs" {
  device_name = var.device_name
  volume_id   = data.aws_ebs_volume.ebs_volume.volume_id
  instance_id = var.instance_id
  stop_instance_before_detaching = true
}

# Should be moved outside of EC2
data "aws_ebs_volume" "ebs_volume" {
  most_recent = true

  filter {
    name = "tag:Name"
    values = ["${var.ebs_volume_name}"]
  }
}